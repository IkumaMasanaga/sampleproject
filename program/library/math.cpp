#include "math.h"

namespace t2k {

	int getSidesPointAndPlane(const Vector3& v, const Vector3& pn, const Vector3& pv)
	{
		Vector3 vec = v - pv;
		float a = vec.dot( pn );
		if (a > 0.0f) 		return 1;		// �\
		else if (a < 0.0f)	return -1;		// ��
		else 				return 0;		// ���ʏ�
	}


	int getRegionPointAndRect(const Vector3& p, const Vector3& rp, const int rect_w, const int rect_h) {
		t2k::Vector3 v1 = t2k::Vector3(rect_w, rect_h, 0).normalize();
		t2k::Vector3 v2 = t2k::Vector3(rect_w, -rect_h, 0).normalize();
		t2k::Vector3 vc1 = v1.cross(t2k::Vector3(0, 0, 1));
		t2k::Vector3 vc2 = v2.cross(t2k::Vector3(0, 0, 1));
		int s1 = t2k::getSidesPointAndPlane(p, vc1, rp);
		int s2 = t2k::getSidesPointAndPlane(p, vc2, rp);
		if (s1 >= 0 && s2 >= 0) {
			return 1;
		}
		else if (s1 >= 0 && s2 <= 0) {
			return 2;
		}
		else if (s1 <= 0 && s2 >= 0) {
			return 0;
		}
		else {
			return 3;
		}
	}

}