#pragma once
#include "../library/t2klib.h"


class Scene;

class GameManager {
private:
	// コンストラクタ
	GameManager() {}

	// デストラクタ
	~GameManager() {}

	Scene* now_ = nullptr;
	Scene* next_ = nullptr;

	t2k::Sequence<GameManager*> seq_ = t2k::Sequence<GameManager*>(this, &GameManager::seqInit);

	// シーケンス処理
	bool seqInit(const float delta_time);	// 初期化
	bool seqIdle(const float delta_time);	// 通常

	// シングルトンなインスタンス
	static GameManager* instance_;

public:
	// 更新
	void update(const float delta_time);

	// 描画
	void render();

	// 現在のシーンの取得
	inline Scene* getNowScene() { return now_; }

	// 次ののシーンの設定
	inline void setNextScene(Scene* next) { next_ = next; }

	// シングルトンなインスタンスの取得
	static GameManager* getInstance();
};
