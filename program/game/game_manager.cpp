#include "game_manager.h"
#include "scene/scene.h"


GameManager* GameManager::instance_ = nullptr;

// 初期化シーケンス
bool GameManager::seqInit(const float delta_time) {

	//----------------//
	// 初期シーン生成 //
	//----------------//

	now_ = next_;

	seq_.change(&GameManager::seqIdle);

	return true;
}

// 通常シーケンス
bool GameManager::seqIdle(const float delta_time) {

	now_->update(delta_time);

	return true;
}

// 更新
void GameManager::update(const float delta_time) {

	if (now_ != next_) {
		if (now_) {
			delete now_;
		}
		now_ = next_;
	}

	seq_.update(delta_time);

}

// 描画
void GameManager::render() {

	now_->render();

}

// シングルトンなインスタンスの取得
GameManager* GameManager::getInstance() {
	if (!instance_) {
		instance_ = new GameManager();
	}
	return instance_;
}
