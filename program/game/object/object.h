#pragma once
#include "../../library/t2klib.h"


class Object {
public:
	Object() {}
	virtual ~Object() {}

	t2k::Vector3 position_;

	virtual void update(const float delta_time) {}
	virtual void render() {}
};
