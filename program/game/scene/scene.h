#pragma once


class Scene {
public:
	Scene() {}
	virtual ~Scene() {}

	virtual void update(const float delta_time) {}
	virtual void render() {}
};
