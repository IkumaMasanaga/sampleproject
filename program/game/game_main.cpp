#include "DxLib.h"
#include "../library/t2klib.h"
#include "../support/Support.h"
#include "game_main.h"
#include "game_manager.h"


void gameMain( float deltatime ) {

	GameManager* mgr = GameManager::getInstance();
	mgr->update(deltatime);
	mgr->render();

}
